#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

register_notification_parameters("pme", Dictionary(
    optional_keys = [],
    elements = [
        ("url", HTTPUrl(
            title = _("PME URL"),
            help = _("Configure the PME URL here."),
            allow_empty = False,
        )),
        ("username", TextAscii(
            title = _("User Name"),
            help = _("Configure the user name here."),
            size = 40,
            allow_empty = False,
        )),
        ("password", Password(
            title = _("Password"),
            help = _("You need to provide a valid password to be able to send notifications."),
            size = 40,
            allow_empty = False,
        )),
    ]
))
